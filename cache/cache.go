package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v8"
	"time"
)

type Cacher interface {
	Set(ctx context.Context, key string, value interface{}) error
	Get(ctx context.Context, key string, out interface{}) error
}

type redisCache struct {
	client *redis.Client
	ttl    time.Duration
}

func NewRedisCache(client *redis.Client, ttl time.Duration) Cacher {
	return &redisCache{client: client, ttl: ttl}
}

func (rc *redisCache) Set(ctx context.Context, key string, value interface{}) error {
	marshalled, err := json.Marshal(value)
	if err != nil {
		return fmt.Errorf("cacher set: %w", err)
	}
	err = rc.client.Set(ctx, key, marshalled, rc.ttl).Err()
	if err != nil {
		return fmt.Errorf("cacher set: %w", err)
	}
	return nil
}

func (rc *redisCache) Get(ctx context.Context, key string, out interface{}) error {
	res, err := rc.client.Get(ctx, key).Result()
	if err != nil {
		return fmt.Errorf("cacher get: %w", err)
	}

	err = json.Unmarshal([]byte(res), out)
	if err != nil {
		return fmt.Errorf("cacher get: %w", err)
	}
	return nil
}
