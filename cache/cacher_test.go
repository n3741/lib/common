package cache

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestCache(t *testing.T) {
	client := redis.NewClient(&redis.Options{
		Addr: "localhost:6379",
		//Username:           "",
		//Password:           "",
		DB: 1,
	})
	ttl := time.Second * 5

	redisCache := NewRedisCache(client, ttl)

	type test struct {
		Id      string
		Name    string
		Surname string
		Age     int
	}

	a := test{Id: "1", Name: "TestName", Surname: "TestSurname", Age: 100}
	key := "first"

	assert.NoError(t, redisCache.Set(context.Background(), key, a))

	var b test
	assert.NoError(t, redisCache.Get(context.Background(), key, &b))
	assert.Equal(t, a, b)
	fmt.Printf("%#v", b)
}
